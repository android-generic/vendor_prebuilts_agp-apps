#
# 2017 Bliss Roms - Adapted from Android-x86 Project
# Original Copyright (C) 2011-2015 The Android-x86 Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#

#~ ifeq ($(TARGET_PC_BUILD),true)

LOCAL_PATH := $(call my-dir)

ifeq ($(USE_TASKBAR_UI),true)

include $(CLEAR_VARS)
LOCAL_MODULE := Taskbar
LOCAL_SRC_FILES := com.farmerbb.taskbar.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SYSTEM_EXT_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := TaskbarSupport
LOCAL_SRC_FILES := com.farmerbb.taskbar.support.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_PRIVILEGED_MODULE := true
LOCAL_SYSTEM_EXT_MODULE := true
include $(BUILD_PREBUILT)

endif
