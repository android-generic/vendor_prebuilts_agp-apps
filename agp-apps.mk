ifeq ($(USE_AGP_WALLPAPER),true)
DEVICE_PACKAGE_OVERLAYS += $(LOCAL_PATH)/overlay
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += $(LOCAL_PATH)/overlay
endif

ifeq ($(INCLUDE_AURORA_SERVICES),true)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/aurora_services/permissions/com.aurora.services.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.aurora.services.xml
endif

ifeq ($(USE_SMARTDOCK),true)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/smart-dock/permissions/cu.axel.smartdock-permissions.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/cu.axel.smartdock-permissions.xml
endif

ifeq ($(USE_BLISS_RESTRICTED_LAUNCHER),true)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/restricted_app/permissions/com.bliss.restrictedlauncher-permissions.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.bliss.restrictedlauncher-permissions.xml
endif

ifeq ($(USE_BLISS_GARLIC_LAUNCHER),true)
# Private permissions
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/garlic_launcher/permissions/com.sagiadinos.garlic.launcher-permissions.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.sagiadinos.garlic.launcher-permissions.xml \
    $(LOCAL_PATH)/garlic_player/permissions/com.sagiadinos.garlic.player-permissions.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.sagiadinos.garlic.player-permissions.xml

endif

ifeq ($(USE_BLISS_GAME_MODE_LAUNCHER),true)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/game_mode/permissions/com.sinu.molla-permissions.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.sinu.molla-permissions.xml
endif

ifeq ($(USE_BLISS_CROSS_LAUNCHER),true)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/crosslauncher/permissions/id.psw.vshlauncher-permissions.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/id.psw.vshlauncher-permissions.xml
endif

ifeq ($(USE_TASKBAR_UI),true)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/taskbar/permissions/privapp-permissions-com.farmerbb.taskbar.support.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-com.farmerbb.taskbar.support.xml \
    $(LOCAL_PATH)/taskbar/permissions/privapp-permissions-com.farmerbb.taskbar.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-com.farmerbb.taskbar.xml
PRODUCT_PACKAGES += Taskbar TaskbarSupport
endif
